package binaryim.android.feedstest;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;


//https://android--examples.blogspot.com/2016/07/android-uninstall-app-programmatically.html
//https://stackoverflow.com/questions/18692571/how-can-an-app-detect-that-its-going-to-be-uninstalled
//https://www.tutorialspoint.com/uninstall-apks-programmatically


public class TestMessage extends BroadcastReceiver  {
    public static boolean is_Uninstalled= false;
    public String TAG = TestMessage.class.getName();
    // DatabaseReference myRef1 = FirebaseDatabase.getInstance().getReference();
    //DatabaseReference myRef = myRef1.child("message");


    public static void s(Context c, String message){
        Toast.makeText(c, message, Toast.LENGTH_SHORT).show();
        //  FireBaseDataRef(pn);
        //Toast.makeText(c,message,Toast.LENGTH_SHORT).show();
    }

    public static void isPackageInstalled(Context c,String username, String packageName, String device_Id,String timestamp, PackageManager packageManager) {
        if(packageName.isEmpty()){
            Toast.makeText(c, "App NOtInstalled ", Toast.LENGTH_SHORT).show();
        }else
            Toast.makeText(c, "Installed "+" w/ "+" Pkg Name "+packageName, Toast.LENGTH_SHORT).show();
        //FireBaseDataRef(username,packageName,device_Id,timestamp);
        new SendDataToApi(username,device_Id,packageName,timestamp).execute("http://appstrack.etreetech.com/api_access/postback");
    }

    public static void TestData(Context c,String model,String device,String gaid,String mac,String a_id,String imei,String pkgnm,
                                String ip_add,String user_id,String user_agent,String manuf){
        if(pkgnm.isEmpty()){
            Toast.makeText(c, "App NotInstalled ", Toast.LENGTH_SHORT).show();
        }else
            Toast.makeText(c, "Installed "+" w/ "+" Pkg Name "+pkgnm, Toast.LENGTH_SHORT).show();
        new SendTestDataz(model,device,gaid,mac,a_id,imei,pkgnm,
                ip_add,user_id,user_agent,manuf).execute("http://appstrack.etreetech.com/track/pb");

    }

    private static void SendData(String username,String packageName,String device_Id,String timestamp) {

        JSONObject urlParams = new JSONObject();
        try {
//            urlParams.put("u", "766ca68d-8110-4f48-b787-701634bfe0c1");
//            urlParams.put("o", "d927e636-04a2-4b7d-b25e-0ec0ab4bc4b7");
//            urlParams.put("agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/75.0.3770.90 Chrome/75.0.3770.90 Safari/537.36");
            urlParams.put("gaid", "username");
            urlParams.put("userId", "packageName");
            urlParams.put("mac", "'device_Id'");
            //  urlParams.put("dynamicId", 1);
            urlParams.put("agent", "timestamp");
            urlParams.put("dynamicId", 1);

            // Log.d("TAG", "SendData  "+username+" mac "+packageName+"gaid "+device_Id+"agent "+ timestamp);

            //Mozilla/5.0 (Linux; Android 8.1.0; Moto G (5S) Plus Build/OPS28.65-36-11; wv
            // ) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.157 Mobile Safari/537.36

            byte[] postData = new byte[0];
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                postData = urlParams.toString().getBytes(StandardCharsets.UTF_8);
            }
            String urly = "http://appstrack.etreetech.com/api_access/postback";
            URL obj = new URL(urly);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
//		System.out.println("Data output link:" + link);
            wr.write(postData);
            wr.flush();
            wr.close();
            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            BufferedReader iny = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String output;
            StringBuffer response = new StringBuffer();

            while ((output = iny.readLine()) != null) {
                response.append(output);

            }
            iny.close();

            String json = response.toString();
            System.out.println(json);
            Log.d("JSON", "SendDataJSON"+json);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private static void SendingDataInUrl() {
        OkHttpClient client = new OkHttpClient();
        String url = "http://panel.etreetech.com/track/tracking?off=10&aff=2&tid=2&gaid=298347928479823742&s1=3223&s2=32&s3=3242";
        Request request = new Request.Builder()
                .url(url)
                .build();

        Log.d("REQUEST", "SendingData " + request.toString());

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {

                final String myResponse = response.body().string();
                Log.d("Response", "onResponse  " + myResponse);

            }
        });
    }

    public static void ParseInstallData(String adIdFinal ,String packageName,String user_agent ,String timestamp,String device_string ,String device_model) {
        //AccountManager am = AccountManager.get(TestMessage.this); // "this" references the current Context
       // Account[] accounts = am.getAccountsByType("com.google");
        //  Log.d("Account ", "SendingData " + Arrays.toString(accounts));
        //  Log.d("Account1 ", "SendingData " + Arrays.toString(am.getAccounts()));


        OkHttpClient client = new OkHttpClient();
        HttpUrl.Builder urlBuilder = HttpUrl.parse("http://panel.etreetech.com/track/tracking?").newBuilder();
        urlBuilder.addQueryParameter("off", "10");
        urlBuilder.addQueryParameter("aff", "2");
        urlBuilder.addQueryParameter("tid", timestamp);
        urlBuilder.addQueryParameter("gaid", adIdFinal);
        urlBuilder.addQueryParameter("s1", device_string);
        urlBuilder.addQueryParameter("s2", device_model);
        urlBuilder.addQueryParameter("s3", packageName);
        urlBuilder.addQueryParameter("s4", user_agent);
        // urlBuilder.addQueryParameter("s5", accounts.toString());

        String url = urlBuilder.build().toString();
        Log.d("URL", "SendingDataURL " + url);

        // String url = "http://panel.etreetech.com/track/tracking?off=10&aff=2&tid=2&gaid=298347928479823742&s1=3223&s2=32&s3=3242";

        Request request = new Request.Builder()
                .url(url)
                .build();

        Log.d("REQUEST", "SendingData " + request.toString());

        //  client.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Custom user agent");
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {

                final String myResponse = response.body().string();
                Log.d("Response", "onResponse  " + myResponse);

            /*    MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("RUN", "run: " + myResponse);
                    }
                });*/
            }
        });
    }

    public static void PuzzleInstallData(String adIdFinal ,String packageName,String user_agent ,String timestamp,String device_string ,String device_model) {
        //AccountManager am = AccountManager.get(TestMessage.this); // "this" references the current Context
       // Account[] accounts = am.getAccountsByType("com.google");
        //  Log.d("Account ", "SendingData " + Arrays.toString(accounts));
        //  Log.d("Account1 ", "SendingData " + Arrays.toString(am.getAccounts()));

       // http://panel.adjarmedia.com/track/tracking?off=19&aff=17&pub_click_id={pub_click_id}&gaid={DEVICE_ID}
        OkHttpClient client = new OkHttpClient();
        HttpUrl.Builder urlBuilder = HttpUrl.parse(" http://panel.adjarmedia.com/track/tracking?").newBuilder();
        urlBuilder.addQueryParameter("off", "19");
        urlBuilder.addQueryParameter("aff", "17");
        urlBuilder.addQueryParameter("pub_click_id", timestamp);
        urlBuilder.addQueryParameter("gaid", adIdFinal);
        urlBuilder.addQueryParameter("s1", device_string);
        urlBuilder.addQueryParameter("s2", device_model);
        urlBuilder.addQueryParameter("s3", packageName);
        urlBuilder.addQueryParameter("s4", user_agent);
        // urlBuilder.addQueryParameter("s5", accounts.toString());

        String url = urlBuilder.build().toString();
        Log.d("URL", "SendingDataURL " + url);

        // String url = "http://panel.etreetech.com/track/tracking?off=10&aff=2&tid=2&gaid=298347928479823742&s1=3223&s2=32&s3=3242";

        Request request = new Request.Builder()
                .url(url)
                .build();

        Log.d("REQUEST", "SendingData " + request.toString());

        //  client.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Custom user agent");
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {

                final String myResponse = response.body().string();
                Log.d("Response", "onResponse  " + myResponse);

            /*    MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("RUN", "run: " + myResponse);
                    }
                });*/
            }
        });
    }

    /* public static void SavingVariableData(Context c,String userId,String device_Id,String time){
        DatabaseReference myRef1 = FirebaseDatabase.getInstance().getReference();
        VariableData Vardata = new VariableData(userId,device_Id,time);
        // myRef1.child("VariableData").push().setValue(Vardata);
        new SendDataToApi(userId,device_Id,"binaryim.android.feedstest",time).execute("http://appstrack.etreetech.com/api_access/postback");
       // SendData(userId,"com.android.testpckg",device_Id,time);

    }

    public static void VariableFunTwo(Context c, String user_id,String device_id,String device_name,String act_name,boolean startorend){//String start_time,String end_time){
        Long tsLong= System.currentTimeMillis() / 1000;
        final String ts = tsLong.toString();
        String Start_Tme = null;

        if(startorend){
            //Activity Start
             Start_Tme = ts;
            DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference();
            VariableTwoCount vartcounRef = new VariableTwoCount(user_id,device_id,device_name,act_name,ts,"0");
            dbRef.child("SecondActivityData").push().setValue(vartcounRef);
        }else{
            //Activity END
            DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference();
            VariableTwoCount vartcounRef = new VariableTwoCount(user_id,device_id,device_name,act_name,Start_Tme,ts);
            dbRef.child("SecondActivityData").push().setValue(vartcounRef);
        }

    }

    public static void PurchaseDetails(Context c,String user_name,String phn_num,String email,String amount){
        DatabaseReference myRef1 = FirebaseDatabase.getInstance().getReference();
        PurchaseData Vardata = new PurchaseData(user_name,phn_num,email,amount);
        myRef1.child("PurchaseData").push().setValue(Vardata);
    }

    private static void FireBaseDataRef(String u_id,String pkg_nm,String device_id,String time) {
        DatabaseReference myRef1 = FirebaseDatabase.getInstance().getReference(); //Getting root reference
        InstallData data = new InstallData(u_id,device_id,pkg_nm,time);
        myRef1.child("users").child(u_id).push().setValue(data);
    }

    private static void setMyRef(DatabaseReference myRef) {
        //this.myRef = myRef;
        myRef.setValue("Intalled");
        myRef.setValue("Time");

    }*/
    @Override
    public void onReceive(Context context, Intent intent) {
        // fetching package names from extras
        String[] packageNames = intent.getStringArrayExtra("android.intent.extra.PACKAGES");
        // String packageNames = intent.getStringArrayExtra("android.intent.extra.PACKAGES");

        if(packageNames!=null){
            for(String packageName: packageNames){
                if(packageName!=null && packageName.equals("binaryim.android.sdktestdata")){
                    // User has selected our application under the Manage Apps settings
                    // now initiating background thread to watch for activity
                    new ListenActivities(context).start();

                }
            }
        }
    }
}

class ListenActivities extends Thread{
    boolean exit = false;
    ActivityManager am = null;
    Context context = null;

    public ListenActivities(Context con){
        context = con;
        am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    }

    public void run(){

        Looper.prepare();

        while(!exit){

            // get the info from the currently running task
            List< ActivityManager.RunningTaskInfo > taskInfo = am.getRunningTasks(MAX_PRIORITY);


            String activityName = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                activityName = Objects.requireNonNull(taskInfo.get(0).topActivity).getClassName();
            }


            Log.d("topActivity", "CURRENT Activity ::"
                    + activityName);

            if (activityName.equals("com.android.packageinstaller.UninstallerActivity")||
                    activityName.equals("com.android.packageinstaller.MainActivity")) {
                //android.intent.action.PACKAGE_REMOVED
                // User has clicked on the Uninstall button under the Manage Apps settings

                //do whatever pre-uninstallation task you want to perform here
                // show dialogue or start another activity or database operations etc..etc..

                // context.startActivity(new Intent(context, MyPreUninstallationMsgActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                exit = true;


                Toast.makeText(context, "Done with preuninstallation tasks... Exiting Now", Toast.LENGTH_SHORT).show();
            } else if(activityName.equals("com.android.settings.ManageApplications")) {
                // back button was pressed and the user has been taken back to Manage Applications window
                // we should close the activity monitoring now
                exit=true;
            }
        }
        Looper.loop();
    }
}

class SendDataToApi extends AsyncTask<String, Void, Integer> {

    private String TAG = SendDataToApi.class.getName();
    private Exception exception;
    private String namee,uID,pkgname,time;

    public SendDataToApi(String u_name,String u_Id,String pkg_name,String timestamp) {
        super();
        namee = u_name;
        uID = u_Id;
        pkgname = pkg_name;
        time=timestamp;
       // ABCD();

    }
    public void ABCD(){
        String url =
                "http://panel.etreetech.com/track/tracking?off=10&aff=2&tid={TID}&gaid={DEVICE_ID}&s1=&s2=&s3=";

        HttpUrl.Builder urlBuilder = HttpUrl.parse("http://panel.etreetech.com/track/tracking?").newBuilder();
        urlBuilder.addQueryParameter("off", "10");
        urlBuilder.addQueryParameter("aff", "2");
        urlBuilder.addQueryParameter("tid", "TID");
        urlBuilder.addQueryParameter("gaid", "GAID");
        String urlrr = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();
    }

    protected Integer doInBackground(String... urls) {

        JSONObject urlParams = new JSONObject();
        try {
            urlParams.put("gaid", namee);
            urlParams.put("userId", uID);
            urlParams.put("mac", pkgname);
            urlParams.put("agent", time);
            urlParams.put("dynamicId", 1);


            byte[] postData = new byte[0];
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                postData = urlParams.toString().getBytes(StandardCharsets.UTF_8);
            }
            Log.d(TAG, "doInBackgroundPostData " + urlParams.toString());

            String urly = "http://appstrack.etreetech.com/api_access/postback";
            URL obj = new URL(urly);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            //		System.out.println("Data output link:" + link);
            wr.write(postData);
            wr.flush();
            wr.close();
            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            BufferedReader iny = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String output;
            // StringBuffer response = new StringBuffer();
            StringBuilder response = new StringBuilder();
            Log.d(TAG, "doInBackgroundJSON " + response.toString());

            while ((output = iny.readLine()) != null) {
                response.append(output);
            }
            //Adjust
            //Appmetric
            //adBrix

            iny.close();
            String json = response.toString();
            System.out.println("JsonResponse"+json);
            Log.d("JSON", "SendDataJSON"+json);

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return 1;
    }

    protected void onPostExecute(Integer feed) {
        // TODO: check this.exception
        // TODO: do something with the feed
    }
}

class VariableCount extends AsyncTask<String, Void, Integer> {

    private String TAG = VariableCount.class.getName();
    private Exception exception;
    private String namee,uID,pkgname,time;

    public VariableCount(String u_name,String u_Id,String pkg_name,String timestamp) {
        super();
        namee = u_name;
        uID = u_Id;
        pkgname = pkg_name;
        time=timestamp;
    }

    protected Integer doInBackground(String... urls) {

        JSONObject urlParams = new JSONObject();
        try {
            urlParams.put("userId", namee);
            urlParams.put("eToken", uID);
            urlParams.put("offerId", pkgname);
            // urlParams.put("agent", time);
            // urlParams.put("dynamicId", 1);


            byte[] postData = new byte[0];
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                postData = urlParams.toString().getBytes(StandardCharsets.UTF_8);
            }
            Log.d(TAG, "doInBackgroundPostData " + urlParams.toString());

            String urly = "http://appstrack.etreetech.com/api_access/genericApi";
            URL obj = new URL(urly);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            //		System.out.println("Data output link:" + link);
            wr.write(postData);
            wr.flush();
            wr.close();
            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            BufferedReader iny = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String output;
            // StringBuffer response = new StringBuffer();
            StringBuilder response = new StringBuilder();
            Log.d(TAG, "doInBackgroundJSON " + response.toString());

            while ((output = iny.readLine()) != null) {
                response.append(output);
            }

            iny.close();
            String json = response.toString();
            System.out.println("JsonResponse"+json);
            Log.d("JSON", "SendDataJSON"+json);

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return 1;
    }

    protected void onPostExecute(Integer feed) {
        // TODO: check this.exception
        // TODO: do something with the feed
    }
}

class SendTestDataz extends AsyncTask<String, Void, Integer> {

    private String TAG = SendInsatllData.class.getName();
    private Exception exception;
    private String _model,_device,_gaid,_mac,_a_id,_imei,_pkgnm,_ip_add,_user_id,_user_agent,_manuf;



    public SendTestDataz(String model,String device,String gaid,String mac,String a_id,String imei,String pkgnm,
                         String ip_add,String user_id,String user_agent,String manuf ) {
        super();
        _model = model;
        _device = device;
        _gaid = gaid;
        _mac=mac;
        _a_id=a_id;
        _imei=imei;
        _pkgnm=pkgnm;
        _ip_add=ip_add;
        _user_id=user_id;
        _user_agent=user_agent;
        _manuf=manuf;
    }

    protected Integer doInBackground(String... urls) {

        JSONObject urlParams = new JSONObject();
        try {


            urlParams.put("ip", _ip_add);
            urlParams.put("mac", _mac);
            urlParams.put("imei", _imei);
            urlParams.put("deviceName", _device);
            urlParams.put("modelName", _model);
            urlParams.put("user_id", _user_id);
            urlParams.put("manufacturer", _manuf);
            urlParams.put("gaid", _gaid);
            urlParams.put("ua", _user_agent);
            urlParams.put("androidId", _a_id);
            urlParams.put("packageName", _pkgnm);
            urlParams.put("offerId", 1);
            //  urlParams.put("dynamicId", 1);


            byte[] postData = new byte[0];
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                postData = urlParams.toString().getBytes(StandardCharsets.UTF_8);
            }
            Log.d(TAG, "doInBackgroundPostData " + urlParams.toString());

            String urly = "http://appstrack.etreetech.com/track/pb";//"http://appstrack.etreetech.com/api_access/postback";
            URL obj = new URL(urly);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            //System.out.println("Data output link:" + link);
            wr.write(postData);
            wr.flush();
            wr.close();
            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            BufferedReader iny = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String output;
            // StringBuffer response = new StringBuffer();
            StringBuilder response = new StringBuilder();
            Log.d(TAG, "doInBackgroundJSON " + response.toString());

            while ((output = iny.readLine()) != null) {
                response.append(output);
            }

            iny.close();
            String json = response.toString();
            System.out.println("JsonResponse"+json);
            Log.d("JSON", "SendDataJSON"+json);

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return 1;
    }

    protected void onPostExecute(Integer feed) {

    }
}
