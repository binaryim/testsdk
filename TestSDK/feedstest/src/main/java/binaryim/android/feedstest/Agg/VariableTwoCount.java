package binaryim.android.feedstest.Agg;

public class VariableTwoCount {
    String user_id;
    String device_id;
    String device_name;
    String act_name;
    String start_time;
    String end_time;
    boolean act_start_end;

    public VariableTwoCount() {

    }

    /*   public VariableTwoCount(String user_id, String device_id, String device_name, String act_name, boolean act_start_end) {
           this.user_id = user_id;
           this.device_id = device_id;
           this.device_name = device_name;
           this.act_name = act_name;
           this.act_start_end = act_start_end;
       }*/
    public VariableTwoCount(String user_id, String device_id, String device_name, String act_name, String start_time, String end_time) {
        this.user_id = user_id;
        this.device_id = device_id;
        this.device_name = device_name;
        this.act_name = act_name;
        this.start_time = start_time;
        this.end_time = end_time;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_name() {
        return device_name;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }

    public String getAct_name() {
        return act_name;
    }

    public void setAct_name(String act_name) {
        this.act_name = act_name;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }
}
