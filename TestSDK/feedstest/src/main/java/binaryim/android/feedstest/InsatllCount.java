package binaryim.android.feedstest;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import binaryim.android.feedstest.Agg.InstallData;

public class InsatllCount {
    TelephonyManager telephonyManager;
    private Long tsLong;// = System.currentTimeMillis() / 1000;
    private String ts;// = tsLong.toString();

    public InsatllCount() {
        tsLong = System.currentTimeMillis()/1000;
        ts = tsLong.toString();
    }

    public static void Beatles(){

    }

    public static void isInstalled(Context c, String username, String packageName, String device_Id, String timestamp, PackageManager packageManager) {
        if(packageName.isEmpty()){
            Toast.makeText(c, "App NotInstalled ", Toast.LENGTH_SHORT).show();
        }else
            Toast.makeText(c, "Installed "+" w/ "+" Pkg Name "+packageName, Toast.LENGTH_SHORT).show();
        InstallData insdata = new InstallData();
        new SendInsatllData(username,packageName,device_Id,timestamp).execute("http://appstrack.etreetech.com/api_access/postback");

    }
    public static void TestData(Context c,String model,String device,String gaid,String mac,String a_id,String imei,String pkgnm){
        if(pkgnm.isEmpty()){
            Toast.makeText(c, "App NotInstalled ", Toast.LENGTH_SHORT).show();
        }else
            Toast.makeText(c, "Installed "+" w/ "+" Pkg Name "+pkgnm, Toast.LENGTH_SHORT).show();
        InstallData insdata = new InstallData();
        new SendTestData(model,device,gaid,mac,a_id,imei,pkgnm).execute("http://appstrack.etreetech.com/api_access/postback");

    }

}
class SendTestData extends AsyncTask<String, Void, Integer> {

    private String TAG = SendInsatllData.class.getName();
    private Exception exception;
    private String _model,_device,_gaid,_mac,_a_id,_imei,_pkgnm;



    public SendTestData(String model,String device,String gaid,String mac,String a_id,String imei,String pkgnm) {
        super();
        _model = model;
        _device = device;
        _gaid = gaid;
        _mac=mac;
        _a_id=a_id;
        _imei=imei;
        _pkgnm=pkgnm;
    }

    protected Integer doInBackground(String... urls) {

        JSONObject urlParams = new JSONObject();
        try {
            urlParams.put("gaid", _gaid);
            urlParams.put("model", _model);
            urlParams.put("mac", _mac);
            urlParams.put("device", _device);
            urlParams.put("imei", _imei);
            urlParams.put("pkgnm", _pkgnm);
            urlParams.put("dynamicId", 1);


            byte[] postData = new byte[0];
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                postData = urlParams.toString().getBytes(StandardCharsets.UTF_8);
            }
            Log.d(TAG, "doInBackgroundPostData " + urlParams.toString());

            String urly = "http://appstrack.etreetech.com/api_access/postback";
            URL obj = new URL(urly);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            //System.out.println("Data output link:" + link);
            wr.write(postData);
            wr.flush();
            wr.close();
            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            BufferedReader iny = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String output;
            // StringBuffer response = new StringBuffer();
            StringBuilder response = new StringBuilder();
            Log.d(TAG, "doInBackgroundJSON " + response.toString());

            while ((output = iny.readLine()) != null) {
                response.append(output);
            }

            iny.close();
            String json = response.toString();
            System.out.println("JsonResponse"+json);
            Log.d("JSON", "SendDataJSON"+json);

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return 1;
    }

    protected void onPostExecute(Integer feed) {

    }
}
class SendInsatllData extends AsyncTask<String, Void, Integer> {

    private String TAG = SendInsatllData.class.getName();
    private Exception exception;
    private String namee,uID,pkgname,time;



    public SendInsatllData(String u_name, String u_Id, String pkg_name, String timestamp) {
        super();
        namee = u_name;
        uID = u_Id;
        pkgname = pkg_name;
        time=timestamp;
    }

    protected Integer doInBackground(String... urls) {

        JSONObject urlParams = new JSONObject();
        try {
            urlParams.put("gaid", namee);
            urlParams.put("userId", uID);
            urlParams.put("mac", pkgname);
            urlParams.put("agent", time);
            urlParams.put("dynamicId", 1);


            byte[] postData = new byte[0];
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                postData = urlParams.toString().getBytes(StandardCharsets.UTF_8);
            }
            Log.d(TAG, "doInBackgroundPostData " + urlParams.toString());

            String urly = "http://appstrack.etreetech.com/api_access/postback";
            URL obj = new URL(urly);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            //System.out.println("Data output link:" + link);
            wr.write(postData);
            wr.flush();
            wr.close();
            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            BufferedReader iny = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String output;
            // StringBuffer response = new StringBuffer();
            StringBuilder response = new StringBuilder();
            Log.d(TAG, "doInBackgroundJSON " + response.toString());

            while ((output = iny.readLine()) != null) {
                response.append(output);
            }

            iny.close();
            String json = response.toString();
            System.out.println("JsonResponse"+json);
            Log.d("JSON", "SendDataJSON"+json);

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return 1;
    }

    protected void onPostExecute(Integer feed) {

    }
}

