package binaryim.android.feedstest.Agg;

public class PurchaseData {
    String user_name;
    String phn_num;
    String email;
    String amount;

    public PurchaseData() {
    }

    public PurchaseData(String user_name, String phn_num, String email, String amount) {
        this.user_name = user_name;
        this.phn_num = phn_num;
        this.email = email;
        this.amount = amount;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPhn_num() {
        return phn_num;
    }

    public void setPhn_num(String phn_num) {
        this.phn_num = phn_num;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
