package binaryim.android.feedstest.Agg;
public class VariableData {

  public String userId;
  public String device_Id;
  public String time;

  public VariableData() {
  }

  public VariableData(String userId, String device_Id, String time) {
    this.userId = userId;
    this.device_Id = device_Id;
    this.time = time;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getDevice_Id() {
    return device_Id;
  }

  public void setDevice_Id(String device_Id) {
    this.device_Id = device_Id;
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }}
