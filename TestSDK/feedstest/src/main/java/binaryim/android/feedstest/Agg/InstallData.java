package binaryim.android.feedstest.Agg;

import com.google.firebase.database.Exclude;


import java.util.HashMap;
import java.util.Map;

public class InstallData {

    public String username;
    public String user_id;
    public String device_id;
    public String ga_id;
    public String package_name;
    public String ip_address;
    public String mac_address;
    public String time;
    public Map<String, Boolean> stars = new HashMap<>();

    public InstallData() {

    }
    public InstallData(String username, String device_id, String package_name, String time) {
        this.username = username;
        this.device_id = device_id;
        this.package_name = package_name;
        this.time = time;
    }



    public InstallData(String username, String user_id, String device_id, String ga_id, String package_name, String ip_address, String mac_address, String time) {
        this.username = username;
        this.user_id = user_id;
        this.device_id = device_id;
        this.ga_id = ga_id;
        this.package_name = package_name;
        this.ip_address = ip_address;
        this.mac_address = mac_address;
        this.time = time;
    }



    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("username", username);
        result.put("device_id", device_id);
        result.put("package_name", package_name);
        result.put("time", time);
        return result;
    }


}
