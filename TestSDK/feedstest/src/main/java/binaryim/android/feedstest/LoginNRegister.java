package binaryim.android.feedstest;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class LoginNRegister {


public static String driverdutydata,logindata,driverRegData;
public static JSONArray loginDataArray;

public LoginNRegister(String data){
    super();
    logindata=data;

}
public LoginNRegister(){

}

    public static String RegisterUser(Context c, String username, String packageName, String licence_num,
                                    String mail_id, String phn_num,String addhar_num,String address,String DOB,String Password) {
        if(packageName.isEmpty()){
            Toast.makeText(c, "App NOtInstalled ", Toast.LENGTH_SHORT).show();
        }else
            Toast.makeText(c, "Installed "+" w/ "+" Pkg Name "+packageName, Toast.LENGTH_SHORT).show();
        new SendRegisterData(username,licence_num,mail_id,phn_num,addhar_num,address,DOB,Password).execute("http://emergency.adstacks.in/driver/register");

   return driverRegData;
    }

    public static String LoginUser(Context c,String packageName,String mail_id, String Password) {
        final String login_user_data=null;
        if(packageName.isEmpty()){
            Toast.makeText(c, "App NOtInstalled ", Toast.LENGTH_SHORT).show();
        }else
            Toast.makeText(c, "Installed "+" w/ "+" Pkg Name "+packageName, Toast.LENGTH_SHORT).show();
        //FireBaseDataRef(username,packageName,device_Id,timestamp);
        new SendLoginData(mail_id,Password).execute("http://emergency.adstacks.in/driver/login");
        Log.d("TAG", "LoginData " + LoginNRegister.logindata + " // " + logindata);

        return logindata;
    }

    public static String DutyRequest(Context c,String packageName,String u_id,String vehicleRegisNum
            ,String locstartstring,String locendstring,String LatStart,String LngStart,String LatEnd,String LngEnd
            ,String time,String user_name,String licenceNumb) {
        if(packageName.isEmpty()){
            Toast.makeText(c, "App NOtInstalled ", Toast.LENGTH_SHORT).show();
        }else
            Toast.makeText(c, "Installed "+" w/ "+" Pkg Name "+packageName, Toast.LENGTH_SHORT).show();
        //FireBaseDataRef(username,packageName,device_Id,timestamp);
        new SendDutyData(u_id,vehicleRegisNum,locstartstring,locendstring,LatStart,LngStart,LatEnd,LngEnd,time,user_name,licenceNumb).execute("http://emergency.adstacks.in/driver/request");

return driverdutydata;
    }

public void DutyData(String dutdata){
    driverdutydata = dutdata;
}

public void LoginData(String data1){
    logindata = data1;
}

public void RegData(String data2){
    driverRegData = data2;
}

}
class SendRegisterData extends AsyncTask<String, Void, Integer> {

    private String TAG = SendDataToApi.class.getName();
    private Exception exception;
    private String namee,licenseNo,mailid,phone,aadharNo,address,dateOfBirth,password,regdata;

    private LoginNRegister loginNRegisterdata= new LoginNRegister();

    public SendRegisterData(String u_name,String lic_num,String mail,String phn_num,String addhar_num
            ,String add,String dob,String pass) {
        super();
        namee = u_name;
        licenseNo = lic_num;
        mailid = mail;
        phone=phn_num;
        aadharNo=addhar_num;
        address=add;
        dateOfBirth=dob;
        password=pass;
        // ABCD();

    }

    protected Integer doInBackground(String... urls) {

        JSONObject urlParams = new JSONObject();
        try {
            urlParams.put("name", namee);
            urlParams.put("licenseNo", licenseNo);
            urlParams.put("mailId", mailid);
            urlParams.put("phone", phone);
            urlParams.put("aadharNo", aadharNo);
            urlParams.put("address", address);
            urlParams.put("dateOfBirth", dateOfBirth);
            urlParams.put("password", password);


            byte[] postData = new byte[0];
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                postData = urlParams.toString().getBytes(StandardCharsets.UTF_8);
            }
            Log.d(TAG, "doInBackgroundPostData " + urlParams.toString());

            String urly = "http://emergency.adstacks.in/driver/register";
            URL obj = new URL(urly);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            //		System.out.println("Data output link:" + link);
            wr.write(postData);
            wr.flush();
            wr.close();
            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            BufferedReader iny = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String output;
            // StringBuffer response = new StringBuffer();
            StringBuilder response = new StringBuilder();
            Log.d(TAG, "doInBackgroundJSON " + response.toString());

            while ((output = iny.readLine()) != null) {
                response.append(output);
            }
            //Adjust
            //Appmetric
            //adBrix

            iny.close();
            String json = response.toString();
            System.out.println("JsonResponse"+json);
            Log.d("JSON", "SendDataJSON"+json);
            regdata=json;
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return 1;
    }

    protected void onPostExecute(Integer feed) {
        // TODO: check this.exception
        // TODO: do something with the feed
        loginNRegisterdata.RegData(regdata);
    }
}

class SendLoginData extends AsyncTask<String, Void, Integer> {

    private String TAG = SendDataToApi.class.getName();
    private Exception exception;
    private String mailid,password,logdata;
   private LoginNRegister loginNRegisterlogin = new LoginNRegister();



    public SendLoginData(String mail,String pass) {
        super();
        mailid = mail;
        password=pass;


    }

    protected Integer doInBackground(String... urls) {

        JSONObject urlParams = new JSONObject();
        try {

            urlParams.put("mailId", mailid);
            urlParams.put("password", password);


            byte[] postData = new byte[0];
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                postData = urlParams.toString().getBytes(StandardCharsets.UTF_8);
            }
            Log.d(TAG, "doInBackgroundPostData " + urlParams.toString());

            String urly = "http://emergency.adstacks.in/driver/login";
            URL obj = new URL(urly);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            //		System.out.println("Data output link:" + link);
            wr.write(postData);
            wr.flush();
            wr.close();
            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            BufferedReader iny = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String output;
            // StringBuffer response = new StringBuffer();
            StringBuilder response = new StringBuilder();
            Log.d(TAG, "doInBackgroundJSON " + response.toString());

            while ((output = iny.readLine()) != null) {
                response.append(output);
            }
            //Adjust
            //Appmetric
            //adBrix

            iny.close();
            String json = response.toString();
            System.out.println("JsonResponse"+json);
            Log.d("JSON", "SendDataJSON"+json);

            logdata=json;

          //  LoginNRegister.logindata = json;//con.getResponseMessage();
           // new LoginNRegister(json);

            /*Log.d(TAG, "LoginData " + LoginNRegister.logindata);

            JSONObject jobj = new JSONObject(json);

            JSONArray array= jobj.getJSONArray("user");

            Log.d(TAG, "LoginDataarray " + array);
           LoginNRegister.loginDataArray = array;


            for(int i=0;i<array.length();i++)
            {
                JSONObject object= array.getJSONObject(i);
               String name = object.getString("name");
                Log.d(TAG, " Username " + name);
            }*/


        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return 1;
    }

    protected void onPostExecute(Integer feed) {
        // TODO: check this.exception
        // TODO: do something with the feed
        loginNRegisterlogin.LoginData(logdata);


    }
}

class SendDutyData extends AsyncTask<String, Void, Integer> {

    private String TAG = SendDutyData.class.getName();
    private Exception exception;
    private String u_id,vehicleRegisNum,locstartstring,locendstring,LatStart,LngStart,LatEnd,LngEnd,time,user_name,
    licenceNumb,dudata;

    private LoginNRegister regiostercall = new LoginNRegister();
    public SendDutyData(String user_id,String vehicleRnum,String startlocstr,String endlocstr
            ,String startLat,String startLng,String endLat,String endLng,String timestamp,String name,String driverLice) {
        super();
        u_id = user_id;
        vehicleRegisNum=vehicleRnum;
        locstartstring=startlocstr;
        locendstring=endlocstr;
        LatStart=startLat;
        LngStart=startLng;
        LatEnd=endLat;
        LngEnd=endLng;
        time=timestamp;
        user_name=name;
        licenceNumb=driverLice;

    }

    protected Integer doInBackground(String... urls) {

        JSONObject urlParams = new JSONObject();
        try {

            urlParams.put("userId", u_id);
            urlParams.put("vehicleRegNumber", vehicleRegisNum);
            urlParams.put("startLocString", locstartstring);
            urlParams.put("endLocString", locendstring);
            urlParams.put("startLat", LatStart);
            urlParams.put("starLng", LngStart);
            urlParams.put("endLat", LatEnd);
            urlParams.put("endLng", LngEnd);
            urlParams.put("timestamp", time);
            urlParams.put("userName", user_name);
            urlParams.put("userLicenceNumber", licenceNumb);


            byte[] postData = new byte[0];
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                postData = urlParams.toString().getBytes(StandardCharsets.UTF_8);
            }
            Log.d(TAG, "doInBackgroundPostData " + urlParams.toString());

            String urly = "http://emergency.adstacks.in/driver/request";
            URL obj = new URL(urly);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            //		System.out.println("Data output link:" + link);
            wr.write(postData);
            wr.flush();
            wr.close();
            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            BufferedReader iny = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String output;
            // StringBuffer response = new StringBuffer();
            StringBuilder response = new StringBuilder();
            Log.d(TAG, "doInBackgroundJSON " + response.toString());

            while ((output = iny.readLine()) != null) {
                response.append(output);
            }
            //Adjust
            //Appmetric
            //adBrix

            iny.close();
            String json = response.toString();
            System.out.println("JsonResponse"+json);
            Log.d("JSON", "SendDataJSON"+json);
            dudata=json;

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return 1;
    }

    protected void onPostExecute(Integer feed) {
        // TODO: check this.exception
        // TODO: do something with the feed
        regiostercall.DutyData(dudata);
    }
}
