package binaryim.android.testsdk;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class MainActivity extends AppCompatActivity{
    private BroadcastReceiver _refreshReceiver = new TestRun();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Uninstall();


    }

    /* BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
         @Override
         public void onReceive(Context context, Intent intent) {
             Intent i = new Intent(MainActivity.this, TestRun.class);
             MainActivity.this.sendBroadcast(i);
         }
     };
 */
    public void Uninstall(){

//        Intent i = new Intent(MainActivity.this, TestRun.class);
//        MainActivity.this.sendBroadcast(i);
//        Intent in = new Intent("ACTION_PACKAGE_REMOVED");
//        sendBroadcast(in);
        Log.d(TAG, "Uninstall");
        Intent intent = new Intent(Intent.ACTION_PACKAGE_REMOVED);
        intent.putExtra("android.intent.extra.PACKAGES","binaryim.android.testsdk");
        intent.putExtra("EXTRA_DATA_REMOVED",Intent.EXTRA_DATA_REMOVED);
        sendBroadcast(intent);
        // intent.setData(Uri.parse("package:" + packageName));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(this._refreshReceiver);
        Intent intent = new Intent(Intent.ACTION_PACKAGE_REMOVED);
        intent.putExtra("android.intent.extra.PACKAGES","binaryim.android.testsdk");
        intent.putExtra("EXTRA_DATA_REMOVED",Intent.EXTRA_DATA_REMOVED);
    }



}
