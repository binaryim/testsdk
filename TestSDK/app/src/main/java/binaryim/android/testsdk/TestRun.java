package binaryim.android.testsdk;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Debug;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class TestRun extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        String[] packageNames = intent.getStringArrayExtra("android.intent.extra.PACKAGES");
       String EXTRA_DATA_REMOVED = intent.getStringExtra("EXTRA_DATA_REMOVED");
        Log.d(TAG, "onReceiveEXTRA_DATA_REMOVED  " + EXTRA_DATA_REMOVED);

        if(packageNames!=null){
            for(String packageName: packageNames){
                if(packageName!=null && packageName.equals("binaryim.android.sdktestdata")||
                        packageName.equals("binaryim.android.testsdk")){
                    // User has selected our application under the Manage Apps settings
                    // now initiating background thread to watch for activity
                    Log.d(TAG, "onReceiveBC " + packageName);
                    new ListenActivities(context).start();

                }
            }
        }
    }
}

class ListenActivities extends Thread{
    boolean exit = false;
    ActivityManager am = null;
    Context context = null;

    public ListenActivities(Context con){
        context = con;
        am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    }

    public void run(){

        Looper.prepare();

        while(!exit){

            // get the info from the currently running task
            List< ActivityManager.RunningTaskInfo > taskInfo = am.getRunningTasks(MAX_PRIORITY);
            String activityName = Objects.requireNonNull(taskInfo.get(0).topActivity).getClassName();
            Log.d("topActivity", "CURRENT Activity ::"
                    + activityName);

            if (activityName.equals("com.android.packageinstaller.UninstallerActivity")||
                    activityName.equals("com.android.packageinstaller.MainActivity")) {
                // User has clicked on the Uninstall button under the Manage Apps settings

                //do whatever pre-uninstallation task you want to perform here
                // show dialogue or start another activity or database operations etc..etc..

                // context.startActivity(new Intent(context, MyPreUninstallationMsgActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                exit = true;
                Toast.makeText(context, "Done with preuninstallation tasks... Exiting Now", Toast.LENGTH_SHORT).show();
            } else if(activityName.equals("com.android.settings.ManageApplications")) {
                // back button was pressed and the user has been taken back to Manage Applications window
                // we should close the activity monitoring now
                exit=true;
            }
        }
        Looper.loop();
    }
}
